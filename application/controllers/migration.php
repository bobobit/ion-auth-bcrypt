<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration extends CI_Controller {

	public function index() {

		$data['messages'] = array();

		$this->load->library('migration');
		if (! $this->migration->current()) {
			$data['messages']['error'] = $this->migration->error_string();
		}
		else {
			$data['messages']['success'] = "Migracijata e uspesna";
		}

		$this->load->view('migration', $data);
	}
}