<?php
if(!function_exists('jsAlert')){
	function jsAlert($message){
		echo "<script type=\"text/javascript\">alert('{$message}');</script>";
	}
}//end fun jsAlert()

if(!function_exists('jsa')){
	function jsa($message){
		echo "<script type=\"text/javascript\">alert('{$message}');</script>";
	}
}//end fun jsa()

if(!function_exists('jsConfirm')){
	function jsConfirm($message=''){
		echo "<script type=\"text/javascript\">confirm('{$message}');</script>";
	}
}//end fun jsAlert()

if(!function_exists('jsConsoleLog')){
	function jsConsoleLog($message){
		echo "<script type=\"text/javascript\">console.log('{$message}');</script>";
	}
}//end fun jsConsoleLog()

if(!function_exists('jsclog')){
	function jsclog($message){
		echo "<script type=\"text/javascript\">console.log('{$message}');</script>";
	}
}//end fun jsConsoleLog()

if(!function_exists('jsdump')){
	function jsdump($message){

/*
$.each(oUpdate[0], function(key, element) {
    console.log('key: ' + key + '\n' + 'value: ' + element);
});
*/

	}
}//end fun jsdump()

if(!function_exists('echo_h1')){
	function echo_h1($message){
		echo "<hr>";
		echo "<h1>{$message}</h1>";
		echo "<hr>";
	}
}//end fun echo_h1()

if(!function_exists('eh1')){
	function eh1($message, $exit=FALSE){
		echo "<hr>";
		echo "<h1>{$message}</h1>";
		echo "<hr>";

		if ($exit) {
			exit;
		}
	}
}//end fun echo_h1()

if(!function_exists('pre_print_r')){
	function pre_print_r($expression){
		echo "<pre>";
		print_r($expression);
		echo "</pre>";
	}
}//end fun pre_print_r()

if(!function_exists('ppr')){
	function ppr($expression, $exit=FALSE){
		echo "<pre>";
		print_r($expression);
		echo "</pre>";

		if ($exit) {
			exit;
		}
	}
}//end fun pre_print_r()

if(!function_exists('remove_all_whsp')){
	function remove_all_whsp($string){
		return preg_replace('/\s+/', '', $string);
	}
}//end fun remove_all_whsp()


if(!function_exists('debug_backtrace_CALLSTACK')){
	function debug_backtrace_CALLSTACK($handle_debug_backtrace){//argument treba da bidi fun debug_backtrace()

// 		$dt = debug_backtrace();
		$dt = $handle_debug_backtrace;
		$cs = '';
		foreach ($dt as $t) {
			$cs .= $t['file'] . ' line ' . $t['line'] . ' function ' . $t['function'] . "()\n";
		}
		
		print_r(nl2br($cs)); exit();
		
	}//end fun print_table_data()
}		

if(!function_exists('debug_backtrace_string')){
	function debug_backtrace_string() {
		$stack = '';
		$i = 1;
		$trace = debug_backtrace();

		unset($trace[0]); //Remove call to this function from stack trace
		foreach($trace as $node) {

			$file = (isset($node['file']))? $node['file'] : '';
			$line = (isset($node['line']))? $node['line'] : '';
			$class = (isset($node['class']))? $node['class'] : '';
			$function = (isset($node['function']))? $node['function'] : '';

			$stack .= '<br>' . "#$i " .$file ."(line: " .$line."): ";
			if(isset($node['class'])) {
				$stack .= $class . "->";
			}
			$stack .= $function . "()" . PHP_EOL;
			$i++;
		}
		return $stack;
	}		
}

function btn_new ($uri)
{
	return anchor($uri, '<i class="glyphicon glyphicon-plus"></i>');
}

function btn_copy ($uri)
{
	return anchor($uri, '<i class="glyphicon glyphicon-copyright-mark"></i>');
}

function btn_edit ($uri)
{
	return anchor($uri, '<i class="glyphicon glyphicon-edit"></i>');
}

function btn_delete_js_confirm ($uri)
{
	return anchor($uri, '<i class="glyphicon glyphicon-remove"></i>', array(
		'onclick' => "return confirm('You are about to delete a record. This cannot be undone. Are you sure?');"
	));
}

function btn_delete ($uri)
{
	return anchor($uri, '<i class="glyphicon glyphicon-remove"></i>');
}



function article_link($article){
	return 'article/' . intval($article->id) . '/' . e($article->slug);
}
function article_links($articles){
	$string = '<ul>';
	foreach ($articles as $article) {
		$url = article_link($article);
		$string .= '<li>';
		$string .= '<h3>' . anchor($url, (e($article->title) . ' <b class="glyphicon glyphicon-arrow-right"></b>')) .  ' </h3>';
		$string .= '<p class="pubdate">' . e($article->pubdate) . '</p>';
		$string .= '</li>';
	}
	$string .= '</ul>';
	return $string;
}

function get_excerpt($article, $numwords = 50){
	$string = '';
	$url = article_link($article);
	$string .= '<h2>' . anchor($url, e($article->title)) .  '</h2>';
	$string .= '<p class="pubdate">' . e($article->pubdate) . '</p>';
	$string .= '<p>' . e(limit_to_numwords(strip_tags($article->body), $numwords)) . '</p>';
	$string .= '<p>' . anchor($url, 'Read more <b class="glyphicon glyphicon-arrow-right"></b>', array('title' => e($article->title))) . '</p>';
	return $string;
}

function limit_to_numwords($string, $numwords){
	$excerpt = explode(' ', $string, $numwords + 1);
	if (count($excerpt) >= $numwords) {
		array_pop($excerpt);
	}
	$excerpt = implode(' ', $excerpt);
	return $excerpt;
}



function e($string){
	return htmlentities($string);
}

function get_menu ($array, $child = FALSE)
{
	$CI =& get_instance();
	$str = '';

	if (count($array)) {
		$str .= $child == FALSE ? '<ul class="nav navbar-nav">' . PHP_EOL : '<ul class="dropdown-menu">' . PHP_EOL;

		foreach ($array as $item) {
				
			$active = $CI->uri->segment(1) == $item['slug'] ? TRUE : FALSE;
						
			if (isset($item['children']) && count($item['children'])) {
				$str .= $active ? '<li class="dropdown active">' : '<li class="dropdown">';
				$str .= '<a  class="dropdown-toggle" data-toggle="dropdown" href="' . site_url(e($item['slug'])) . '">' . e($item['name']);
				$str .= '<b class="caret"></b></a>' . PHP_EOL;
				$str .= get_menu($item['children'], TRUE);
			}
			else {
				$str .= $active ? '<li class="active">' : '<li>';
				$str .= '<a href="' . site_url($item['slug']) . '">' . e($item['name']) . '</a>';
			}
			$str .= '</li>' . PHP_EOL;
		}

		$str .= '</ul>' . PHP_EOL;
	}

	return $str;
}

function get_admin_menu ($array, $child = FALSE){

	$CI =& get_instance();
	$str = '';

	if (count($array)) {
		$str .= $child == FALSE ? '<ul class="nav navbar-nav">' . PHP_EOL : '<ul class="dropdown-menu">' . PHP_EOL;

// ppr($array, 1);

		foreach ($array as $key => $item) {
				
			$active = $CI->uri->segment(1) == $item['slug'] ? TRUE : FALSE;
						
			if (isset($item['children']) && count($item['children'])) {
				$str .= $active ? '<li class="dropdown active">' : '<li class="dropdown">';
				$str .= '<a  class="dropdown-toggle" data-toggle="dropdown" href="' . site_url(e('admin/'.$item['slug'])) . '">';
				$str .= $CI->config->config['department_icons'][$item['slug']]  . PHP_EOL;
				$str .= e($item['name']);
				$str .= '<b class="caret"></b></a>' . PHP_EOL;
				$str .= get_admin_menu($item['children'], TRUE);
			}
			else {
				$str .= $active ? '<li class="active">' : '<li>';
				$str .= '<a href="' . site_url('admin/'.$item['slug']) . '">' . e($item['name']) . '</a>';
			}
			$str .= '</li>' . PHP_EOL;
			
			if ($child == TRUE) {
				$last_key = end(array_keys($array));
				if ($key == $last_key) {
					/*Edit Menue*/
					$str .= '<li class="divider"></li>' . PHP_EOL;
					$str .= '<li><a href="' . site_url('admin/page/edit/' . $array[0]['template']) . '">' . PHP_EOL;
					$str .= ' <span class="glyphicon glyphicon-edit"></span> ' . PHP_EOL;
					$str .= 'Edit Menu ' . ucfirst($array[0]['template']) . ' </a></li>' . PHP_EOL;

					/*Reorder Menue*/
					$str .= '<li class="divider"></li>' . PHP_EOL;
					$str .= '<li><a href="' . site_url('admin/page/order/' . $array[0]['template']) . '">' . PHP_EOL;
					$str .= ' <span class="glyphicon glyphicon-sort"></span> ' . PHP_EOL;
					$str .= 'Reorder Menu ' . ' </a></li>' . PHP_EOL;

					/*New Category*/
					$str .= '<li class="divider"></li>' . PHP_EOL;
					$str .= '<li><a href="' . site_url('admin/page/add/' . $array[0]['template']) . '">' . PHP_EOL;
					$str .= ' <span class="glyphicon glyphicon-plus"></span> ' . PHP_EOL;
					$str .= 'New Category</a></li>' . PHP_EOL;
				}
				
			}
		}/*end foreach*/

		$str .= '</ul>' . PHP_EOL;
	}

	return $str;
}

function department_links($grupped_pages, $department){

	$str = '';

	$links = $grupped_pages[$department];

	foreach ($links as $link) {
		$str .= '<a class="pull-right hidden-xs" href="';
		$str .= $link;
		$str .= '">';
		$str .= '<span class="badge">';
		$str .= $link;
		$str .= '</span>';
		$str .= '</a>';
	}

	return $str;
}


/**
 * Dump helper. Functions to dump variables to the screen, in a nicley formatted manner.
 * @author Joost van Veen
 * @version 1.0
 */
if (!function_exists('dump')) {
	function dump ($var, $label = 'Dump', $echo = TRUE)
	{
		// Store dump in variable
		ob_start();
		var_dump($var);
		$output = ob_get_clean();

		// Add formatting
		$output = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $output);
		$output = '<pre style="background: #FFFEEF; color: #000; border: 1px dotted #000; padding: 10px; margin: 10px 0; text-align: left;">' . $label . ' => ' . $output . '</pre>';

		// Output
		if ($echo == TRUE) {
			echo $output;
		}
		else {
			return $output;
		}
	}
}


if (!function_exists('dump_exit')) {
	function dump_exit($var, $label = 'Dump', $echo = TRUE) {
		dump ($var, $label, $echo);
		exit;
	}
}

if (!function_exists('get_id_qty_pair_objects')) {
	function get_id_qty_pair_objects($id_qty_str) {

		if ($id_qty_str == '') return;

		$array_dash = explode(',', $id_qty_str);
		$id_qty_pairs = array();

		foreach ($array_dash as $pair_dash) {
			$pair = explode('-', $pair_dash);
			$obj = new stdClass();
			$atr_id = 'id';
			$obj->$atr_id = $pair[0];
			$atr_qty = 'qty';
			$obj->$atr_qty = $pair[1];
			$id_qty_pairs[] = $obj;
		}

		return $id_qty_pairs;
	}
}

if (!function_exists('get_id_qty_pair_array')) {
	function get_id_qty_pair_array($id_qty_str) {

		if ($id_qty_str == '') return;

		$array_dash = explode(',', $id_qty_str);
		$id_qty_pairs = array();

		foreach ($array_dash as $pair_dash) {
			$pair = explode('-', $pair_dash);
			$id_qty_pairs[$pair[0]] = $pair[1];
		}

		return $id_qty_pairs;
	}
}

if (!function_exists('unique_id_qty_pair_array')) {
	function unique_id_qty_pair_array($old, $new) {

		for ($i=0; $i < sizeof($old); $i++) { 
			for ($j=0; $j < sizeof($new); $j++) { 
				if ($old[$i]->id == $new[$j]->id) {
					$old[$i]->qty = $new[$j]->qty;
					$faunded[] = $j;
				}
			}
		}

		foreach ($faunded as $k) {
			$new[$k] = NULL;
		}

		foreach ($new as $n) {
			if ($n !== NULL) {
				$old[] = $n;
			}
		}

		return $old;
/*old ama prerabotena 
mozi treba da go smenam imeto ili da kreiram nova rezultantna niza*/

	}


if (!function_exists('implode_array_of_obj')) {
	function implode_array_of_obj($delimiter, $array_objects, $attr) {

		foreach ($array_objects as $obj) {
			$array[] = $obj->$attr;
		}

/*		if (!empty($array)) {
			return implode($delimiter, $array);
		}*/

		return implode($delimiter, $array);
	}
}
/*
if (!function_exists('date_valid')) {
	function date_valid($date)  {
	    $parts = explode("/", $date);
	    if (count($parts) == 3) {      
	      if (checkdate($parts[1], $parts[0], $parts[2])){
	        return TRUE;
	      }
	    }
	    // $this->form_validation->set_message('date_valid', 'The Date field must be mm/dd/yyyy');
	    return false;
	}
}*/


if (!function_exists('object_to_array')) {
	function object_to_array($d) {
		if (is_object($d)) {
	        // Gets the properties of the given object
	        // with get_object_vars function
	        $d = get_object_vars($d);
	    }

	    if (is_array($d)) {
	        /*
	        * Return array converted to object
	        * Using __FUNCTION__ (Magic constant)
	        * for recursive call
	        */
	        return array_map(__FUNCTION__, $d);
	    } else {
	        // Return array
	        return $d;
	    }
	}
}


if (!function_exists('get_user_IP')) {
	function get_user_IP() {
		// Get user IP address
		if ( isset($_SERVER['HTTP_CLIENT_IP']) && ! empty($_SERVER['HTTP_CLIENT_IP'])) {
		    $ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif ( isset($_SERVER['HTTP_X_FORWARDED_FOR']) && ! empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
		    $ip = (isset($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0';
		}

		$ip = filter_var($ip, FILTER_VALIDATE_IP);
		return ($ip === false) ? '0.0.0.0' : $ip;
	}
}

if (!function_exists('getClientIP')) {
	function getClientIP() {

	    if (isset($_SERVER)) {

	        if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
	            return $_SERVER["HTTP_X_FORWARDED_FOR"];

	        if (isset($_SERVER["HTTP_CLIENT_IP"]))
	            return $_SERVER["HTTP_CLIENT_IP"];

	        return $_SERVER["REMOTE_ADDR"];
	    }

	    if (getenv('HTTP_X_FORWARDED_FOR'))
	        return getenv('HTTP_X_FORWARDED_FOR');

	    if (getenv('HTTP_CLIENT_IP'))
	        return getenv('HTTP_CLIENT_IP');

	    return getenv('REMOTE_ADDR');
	}
}

if (!function_exists('get_IP_address')) {
	function get_IP_address()
	{
	    foreach (array('HTTP_CLIENT_IP',
	                   'HTTP_X_FORWARDED_FOR',
	                   'HTTP_X_FORWARDED',
	                   'HTTP_X_CLUSTER_CLIENT_IP',
	                   'HTTP_FORWARDED_FOR',
	                   'HTTP_FORWARDED',
	                   'REMOTE_ADDR') as $key){
	        if (array_key_exists($key, $_SERVER) === true){
	            foreach (explode(',', $_SERVER[$key]) as $IPaddress){
	                $IPaddress = trim($IPaddress); // Just to be safe

	                if (filter_var($IPaddress,
	                               FILTER_VALIDATE_IP,
	                               FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE)
	                    !== false) {

	                    return $IPaddress;
	                }
	            }
	        }
	    }
	}
}

if (!function_exists('mysql_date_format')) {
	function mysql_date_format($date_str) {
		/*  date_str php
			0 - m
			1 - d
			2 - y
		*/
		$parts = explode("/", $date_str);
	    if (count($parts) == 3) {      
	      if (checkdate($parts[0], $parts[1], $parts[2])){

	      	$date = new DateTime();
			$date->setDate($parts[2], $parts[0], $parts[1]);

	        return $date->format('Y-m-d');
	      }
	    }
	    return $date_str;
	}
}

if (!function_exists('php_date_format')) {
	function php_date_format($date_str) {
		/*  date_str mysql
			0 - y
			1 - m
			2 - d
		*/
		$parts = explode("-", $date_str);

	    if (count($parts) == 3) {  
	      /*  checkdate( int $month, int $day , int $year )   */    
	      if (checkdate($parts[1], $parts[2], $parts[0])){

	      	$date = new DateTime();
			$date->setDate($parts[0], $parts[1], $parts[2]);

	        return $date->format('m/d/Y');
	      }
	    }
	    return $date_str;
	}
}





/*END*/}